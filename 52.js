"use strict";

/**
 * node 52.js 2017
 */
require('./date-get-week');

const fs = require("fs");
const year = process.argv[2] || new Date().getFullYear();
const fileName = `./52-${year}.txt`;
const endOfYear = `12-31-${year}`;

// lines not started with aterisk (but not empty) are the books, right?
const lineNotStartedWithAterisk = /^[^\*].*/

fs.readFile(fileName, '', function(error, data) {
  if (error)
    return console.log(error);

  const linesOfText = data.toString().split('\n').filter(function(item) {
    return lineNotStartedWithAterisk.test(item);
  });

  const weeks = new Date(endOfYear).getWeek();
  const tpl = "book #book of #weeks in #week week";
  let info = tpl.replace('#book', linesOfText.length)
    .replace('#weeks', weeks)
    .replace('#week', new Date().getWeek());

  if ( weeks === linesOfText.length) {
    console.log(`Success in ${year}`);
  } else {
    console.log(info);
  }

});
