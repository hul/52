1. Jim Butcher, Front Burzowy
2. Jim Butcher, Pełnia Księżyca
3. Jim Butcher, Grave Peril
4. Dan Simmons, Hyperion
5. Dan Simmons, Upadek Hyperiona
6. Waldemar Łysiak, Dobry
7. C.S. Lewis, Lew, Czarownica i Stara Szafa
8. J.A. Zajdel, Cylinder van Troffa
9. Nik Pierumow, Ostrze Elfów
10. Nik Pierumow, Czarna Włócznia
11. Jim Butcher, Rycerz Lata
12. Tom Hughes-Croucher/Mike Wilson, Node: Up and Running
13. Jonathan Snook, Scalable and Modular Architecture for CSS
14. C.S. Lewis, Książe Kaspian
15. H.P. Lovecraft, Zew Cthulu
16. Michael Crichton, Andromeda znaczy śmierć
17. Kevin M. Conolly, Drugi Rzut Oka
18. Henryk Sienkiewicz, Quo Vadis
19. C.S. Lewis, Wędrowiec do Świtu
20. A. Pilipiuk, Litr Ciekłego Ołowiu
21. J.A. Zajdel, Cala Prawda O Planecie Ksi
22. Jim Butcher, Death Masks
23. T. Pratchett, Para W Ruch
24. Jim Butcher, Krwawe Rytuały
25. T. Pratchett, Pasterska Korona
26. Jim Butcher, Martwy Rewir
27. Jim Butcher, Dowody Winy
28. John Maddox Roberts, Śledztwo Decjusza
29. Simon Beckett, Chemia Śmierci
30. J.A. Zajdel, Wyjście z Cienia
31. Roger Zelazny, Dziewięciu książąt Amberu
32. Roger Zelazny, Karabiny Avalonu
33. Roger Zelazny, Znak Jednorozca
34. Roger Zelazny, Ręka Oberona
35. Roger Zelazny, Dworce Chaosu
36. C.S. Lewis, Srebrne Krzesło
37. Simon Beckett, Zapisane w kościach
38. E. Cherezińska, Niewidzalna Korona
39. Jake Spurlock, Bootstrap, Responsive Web Development
40. Simon Beckett, Martwe Szepty
41. Jim Butcher, Akta Dresdena: Biała Noc
42. Roger Zelazny, Atuty Zguby
43. Roger Zelazny, Krew Amberu
44. Roger Zelazny, Znak Chaosu
45. Roger Zelazny, Rycerz Cieni
46. Valdemar Baldhead, Perfidia
47. Roger Zelazny, Książę Chaosu
48. E. Cherezińska, Gra w kości
49. J. Korwin-Mikke, Vademecum Ojca
50. C.S. Lewis, Koń i jego chłopiec
51. A. Pilipuik, Percepcja
52. C.S. Lewis, Siostrzeniec Czarodzieja

* C.S. Lewis Ostatnia Bitwa
* Tolkien, Niedokończone Opowieści
* Charles Duhigg, Siła Nawyku
* Arduino and LEGO Projects
