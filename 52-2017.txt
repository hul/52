Andrzej Pilipiuk, Konan Destylator
Simon Becket, Wołanie Grobu
Jo Nesbo, Człowiek Nietoperz
Piotr Zychowicz, Sowieci
Basarat Ali Syed, TypeScript Deep Dive
John Maddox Roberts, Sprzysięenie Katyliny
John Maddox Roberts, Detektyw Cezara
J.R.R Tolkien, Hobbit
John Maddox Roberts, Świątynia Muz
Antoine de Saint-Exupéry, Mały Książę
John Maddox Roberts, Saturnalia

* Mark Twain, Przygody Tomka Sawyera
* Scott Chacon, Ben Straub, Pro Git 2nd Edition
* Rangle's Angular Training Book
* Denis Stoyanov, RxJS v4.0